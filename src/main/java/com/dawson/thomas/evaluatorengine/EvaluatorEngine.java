package com.dawson.thomas.evaluatorengine;

import com.dawson.thomas.evaluatorengine.exceptions.*;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.NoSuchElementException;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class used for evaluating mathematical expressions in infix notation and
 * returning the result. This is accomplished via the only public method:
 * evaluate(). Internally, the class converts the infix expression, which is
 * split into a queue of Strings, into a postfix expression which it then
 * calculates. This is done to make preserving the order of operations (i.e.
 * BEDMAS) easier. This evaluator can handle addition, subtraction,
 * multiplication, division, and expression containing parenthesis.
 *
 * @author Thomas Ouellette
 */
public class EvaluatorEngine {

    private final static Logger LOG = LoggerFactory.getLogger(EvaluatorEngine.class);

    public EvaluatorEngine() {
        super();
    }

    /**
     * Takes a mathematical expression in infix notation that has been split
     * into a queue of Strings and evaluates it from left to right in the BEDMAS
     * order of operations. The infix expression is converted into an equivalent
     * postfix expression, which is then evaluated, and its result returned.
     * This evaluator can handle decimal numbers; and the following symbols: +,
     * -, /, *, ().
     *
     * @param infixQueue a mathematical expression expressed as a queue of
     * Strings.
     * @return the result of the expression. If the infix expression is null or 
     *         empty, null is returned.
     * @throws NonBinaryExpressionException
     * @throws NonMatchingParenthesisException
     * @throws InvalidStringException
     * @throws DivisionByZeroException
     * @see convertInfixToPostfix()
     * @see evaluatePostfix()
     */
    public String evaluate(Queue<String> infixQueue) throws NonBinaryExpressionException, NonMatchingParenthesisException, InvalidStringException, DivisionByZeroException {
        if(infixQueue == null || infixQueue.isEmpty()){
            return null;
        }
        LOG.debug("Infix expression: " + Arrays.toString(infixQueue.toArray()));
        Queue<String> postfix = convertInfixToPostfix(infixQueue);
        LOG.debug("Postfix expression: " + Arrays.toString(postfix.toArray()));

        return evaluatePostfix(postfix);
    }

    /**
     * Takes an infix expression split up into a queue of Strings and converts
     * it into an equivalent postfix expression.
     *
     * @param infixQueue the infix expression to convert.
     * @return an equivalent postfix expression. Returns an empty Queue if the 
     *         infix queue is null or empty.
     * @throws NonBinaryExpressionException
     * @throws NonMatchingParenthesisException
     */
    private Queue<String> convertInfixToPostfix(Queue<String> infixQueue) throws NonBinaryExpressionException, NonMatchingParenthesisException, InvalidStringException {
        LOG.debug("Converting infix expression to postfix expression...");

        Deque<String> operatorsStack = new ArrayDeque<>();
        Queue<String> postfixQueue = new ArrayDeque<>();

        String previousValue = null;
        
        if(infixQueue == null || infixQueue.isEmpty()){
            return postfixQueue;
        }

        while (!infixQueue.isEmpty()) {
            String currentValue = infixQueue.poll();
            LOG.debug("Current value: " + currentValue + "\tprevious value: " + previousValue);

            if (currentValue != null) {
                if (isDouble(currentValue)) {
                    if (previousValue == null || isOperator(previousValue) || previousValue.compareTo(")") == 0) {
                        if (previousValue != null && previousValue.compareTo(")") == 0) {
                            resolveOperator(operatorsStack, postfixQueue, "*");
                        }
                        postfixQueue.add(currentValue);
                        previousValue = currentValue;
                    } else {
                        throw new NonBinaryExpressionException();
                    }
                } else if (isOperator(currentValue)) {
                    if (previousValue == null || isOperator(previousValue)) {
                        throw new NonBinaryExpressionException();
                    } else {
                        resolveOperator(operatorsStack, postfixQueue, currentValue);
                        previousValue = currentValue;
                    }
                } else if (currentValue.compareTo("(") == 0) {
                    if (previousValue != null && !isOperator(previousValue)) {
                        resolveOperator(operatorsStack, postfixQueue, "*");
                    }

                    /*
                    Expression inside the parenthesis is retrieved (excludes the 
                    opening and closing parenthesis). A recursive call is made
                    to get the expression inside the parenthesis as a postfix 
                    expression. Recursion is used to handle the problem of there 
                    being nested parenthesis.
                     */
                    Queue<String> subPostfixQueue = convertInfixToPostfix(getSubInfix(infixQueue));

                    /*
                    Adds the entire expression inside of the parenthesis 
                    to the main postfix queue.
                     */
                    while (!subPostfixQueue.isEmpty()) {
                        postfixQueue.add(subPostfixQueue.poll());
                    }

                    /*
                    Once getSubInfix method finds the closing parenthesis, it returns. 
                    Therefore, the following line is required for this method to know 
                    that the last value was a closing parenthesis.
                     */
                    previousValue = ")";
                } else if (currentValue.compareTo(")") == 0) {
                    throw new NonMatchingParenthesisException();
                } else {
                    LOG.error("Invalid string: " + currentValue);
                    throw new InvalidStringException();
                }
            } else {
                LOG.error("Null String in Queue");
                throw new InvalidStringException();
            }
        }

        while (!operatorsStack.isEmpty()) {
            postfixQueue.add(operatorsStack.pop());
        }

        return postfixQueue;
    }

    /**
     * Takes a mathematical expression in postfix notation and calculates its
     * result.
     *
     * @param postfixQueue the postfix expression to evaluate.
     * @return the result of the expression. If the postfix Queue is empty, 
     *         it returns null.
     * @throws InvalidStringException
     * @throws DivisionByZeroException
     * @throws NonBinaryExpressionException
     */
    private String evaluatePostfix(Queue<String> postfixQueue) throws InvalidStringException, DivisionByZeroException, NonBinaryExpressionException {
        LOG.debug("Evaluating postfix expression...");
        Deque<String> operandsStack = new ArrayDeque<>();

        while (!postfixQueue.isEmpty()) {
            String currentValue = postfixQueue.poll();

            if (isDouble(currentValue)) {
                operandsStack.push(currentValue);
            } else if (isOperator(currentValue)) {
                String numberOnTheRight = null;
                String numberOnTheLeft = null;
                try {
                    numberOnTheRight = operandsStack.pop();
                    numberOnTheLeft = operandsStack.pop();

                    double result = calculate(new String[]{numberOnTheLeft, currentValue, numberOnTheRight});

                    operandsStack.push(Double.toString(result));
                } catch (NoSuchElementException nsee) {
                    LOG.error("Could not perform operation: " + numberOnTheLeft + " " + currentValue + " " + numberOnTheRight);
                    throw new NonBinaryExpressionException();
                }
            } else {
                LOG.error("Invalid String: " + currentValue);
                throw new InvalidStringException();
            }
        }

        String result = operandsStack.isEmpty() ? null:operandsStack.pop();
        LOG.debug("Result = " + result);
        return result;
    }

    /**
     * Takes an array of 3 strings: 2 numbers and 1 operator in infix notation.
     * Validates and the calculates the value of the expression and returns it.
     *
     * @param expression the binary expression to be calculated.
     * @return the value of the expression.
     * @throws DivisionByZeroException
     * @throws NonBinaryExpressionException
     * @throws InvalidStringException
     * @see performCalculation()
     */
    private double calculate(String[] expression) throws DivisionByZeroException, NonBinaryExpressionException, InvalidStringException {
        if (expression.length == 3) {
            String operator = expression[1];
            String numberOnTheLeft = expression[0];
            String numberOnTheRight = expression[2];
            if (isDouble(numberOnTheLeft) && isDouble(numberOnTheRight) && isOperator(operator)) {
                return performCalculation(Double.parseDouble(numberOnTheLeft), Double.parseDouble(numberOnTheRight), operator);
            } else {
                LOG.error("Values in expression should be in the order: <number>,<operator>,<number>. Expression: " + Arrays.toString(expression));
                throw new InvalidStringException();
            }
        } else {
            LOG.error("Too many values in expression. Should only be 3, had: " + expression.length);
            throw new NonBinaryExpressionException();
        }
    }

    /**
     * Takes 2 numbers and an operator, calculates the result by doing the
     * following: left (operator) right = result
     *
     * @param left number on the left side of the operator.
     * @param right number on the right side of the operator.
     * @param operator the operator to use in the calculation.
     * @return the result of the calculation.
     * @throws DivisionByZeroException
     * @throws InvalidStringException
     */
    private double performCalculation(double left, double right, String operator) throws DivisionByZeroException, InvalidStringException {
        if (operator.compareTo("+") == 0) {
            return left + right;
        } else if (operator.compareTo("-") == 0) {
            return left - right;
        } else if (operator.compareTo("/") == 0) {
            if (right != 0) {
                return left / right;
            } else {
                throw new DivisionByZeroException();
            }
        } else if (operator.compareTo("*") == 0) {
            return left * right;
        } else {
            LOG.error("Invalid operator: " + operator);
            throw new InvalidStringException();
        }
    }

    /**
     * Used to determine the precedence of different operators. Multiplication
     * and division have the same priority, as do addition and subtraction
     *
     * @param operator
     * @return
     */
    private int getPrecedence(String operator) throws InvalidStringException {
        if (operator.compareTo("+") == 0) {
            return 0;
        }
        if (operator.compareTo("-") == 0) {
            return 0;
        }
        if (operator.compareTo("*") == 0) {
            return 1;
        }
        if (operator.compareTo("/") == 0) {
            return 1;
        } else {
            LOG.error("\"" + operator + "\" is not an operator.");
            throw new InvalidStringException();
        }
    }

    /**
     * Method used to determine whether or not a given String is a
     * number/double.
     *
     * @param string the String to check.
     * @return {@code true} if the string is a double, {@code false} otherwise.
     */
    private boolean isDouble(String string) {
        if (string == null) {
            return false;
        }
        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Checks if a String is an operator that this calculator can handle.
     *
     * @param string the string to check.
     * @return {@code true} if the string is a, operator, {@code false}
     * otherwise.
     */
    private boolean isOperator(String string) {
        if (string == null) {
            return false;
        }
        return string.compareTo("+") == 0
                || string.compareTo("-") == 0
                || string.compareTo("*") == 0
                || string.compareTo("/") == 0;
    }

    /**
     * Compares the operator to the operators on the operator stack. Takes
     * operators on the operator stack with a higher precedence and adds them to
     * the postfix expression. Once the top of the stack has a lower precedence,
     * or is empty, the operator is added onto the top of the stack.
     *
     * @param operatorsStack the stack holding the operators.
     * @param postfix the postfix expression to add operators to.
     * @param operator the operator whose placement is being resolved.
     * @throws InvalidStringException
     * @see getPrecedence()
     */
    private void resolveOperator(Deque<String> operatorsStack, Queue<String> postfix, String operator) throws InvalidStringException {
        while (!operatorsStack.isEmpty() && (getPrecedence(operator) <= getPrecedence(operatorsStack.peek()))) {
            postfix.add(operatorsStack.pop());
        }
        operatorsStack.push(operator);
    }

    /**
     * Gets the expression inside of a set of parenthesis. Returns when the
     * corresponding closing parenthesis is found. Assumes an opening
     * parenthesis has already been found in the queue.
     *
     * @param mainInfix the main infix queue containing the expression in
     * parenthesis.
     * @return only the part of the expression between the parenthesis (not
     * including the opening or closing parenthesis).
     * @throws NonMatchingParenthesisException
     */
    private Queue<String> getSubInfix(Queue<String> mainInfix) throws NonMatchingParenthesisException {
        Queue<String> subInfixQueue = new ArrayDeque<>();
        int openParenthesis = 1;
        while (!mainInfix.isEmpty()) {
            String currentValue = mainInfix.poll();

            if (currentValue.compareTo("(") == 0) {
                openParenthesis++;
                subInfixQueue.add(currentValue);
            } else if (currentValue.compareTo(")") == 0) {
                openParenthesis--;
                if (openParenthesis == 0) {
                    break;
                } else {
                    subInfixQueue.add(currentValue);
                }
            } else {
                subInfixQueue.add(currentValue);
            }
        }

        if (openParenthesis != 0) {
            throw new NonMatchingParenthesisException();
        }

        return subInfixQueue;
    }
}
