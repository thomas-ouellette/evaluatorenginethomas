package com.dawson.thomas.evaluatorengine.exceptions;

/**
 *
 * @author Thomas
 */
public class NonBinaryExpressionException extends Exception{
    
    private static final long serialVersionUID = 1L;
    
    public NonBinaryExpressionException(){
        super();
    }
    
    public NonBinaryExpressionException(String message){
        super(message);
    }
    
    public NonBinaryExpressionException(String message, Throwable cause){
        super(message, cause);
    }
    
    public NonBinaryExpressionException(Throwable cause){
        super(cause);
    }
}
