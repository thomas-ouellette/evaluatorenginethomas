package com.dawson.thomas.evaluatorengine.exceptions;

/**
 *
 * @author Thomas
 */
public class DivisionByZeroException extends Exception{
    
    private static final long serialVersionUID = 1L;
    
    public DivisionByZeroException(){
        super();
    }
    
    public DivisionByZeroException(String message){
        super(message);
    }
    
    public DivisionByZeroException(String message, Throwable cause){
        super(message, cause);
    }
    
    public DivisionByZeroException(Throwable cause){
        super(cause);
    }
}

