package com.dawson.thomas.evaluatorengine.exceptions;

/**
 *
 * @author Thomas
 */
public class NonMatchingParenthesisException extends Exception{
    
    private static final long serialVersionUID = 1L;
    
    public NonMatchingParenthesisException(){
        super();
    }
    
    public NonMatchingParenthesisException(String message){
        super(message);
    }
    
    public NonMatchingParenthesisException(String message, Throwable cause){
        super(message, cause);
    }
    
    public NonMatchingParenthesisException(Throwable cause){
        super(cause);
    }
}
