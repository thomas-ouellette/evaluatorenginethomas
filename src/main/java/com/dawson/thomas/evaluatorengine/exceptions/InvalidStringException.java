package com.dawson.thomas.evaluatorengine.exceptions;

/**
 *
 * @author Thomas
 */
public class InvalidStringException extends Exception{
    
    private static final long serialVersionUID = 1L;
    
    public InvalidStringException(){
        super();
    }
    
    public InvalidStringException(String message){
        super(message);
    }
    
    public InvalidStringException(String message, Throwable cause){
        super(message, cause);
    }
    
    public InvalidStringException(Throwable cause){
        super(cause);
    }
}
