package com.dawson.thomas.evaluatorengine;

import com.dawson.thomas.evaluatorengine.exceptions.NonMatchingParenthesisException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 3 test cases for the NonMatchingParenthesisException
 * 
 * @author Thomas
 */
@RunWith(Parameterized.class)
public class EvaluatorEngineParenthesisMismatchTest {
    
    private static final Logger LOG = LoggerFactory.getLogger(EvaluatorEngineParenthesisMismatchTest.class);
    
    @Parameterized.Parameters(name = "{index} expression[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            
            {new ArrayDeque<>(Arrays.asList("(", "2", "+", "3", "*", "4"))},
            {new ArrayDeque<>(Arrays.asList("2", "-", "3", "+", "2", ")"))},
            {new ArrayDeque<>(Arrays.asList("(", "5", "*", "25", "+", "(", "-5", ")", "-", "20", "/", "2"))}
        });
    }
    
    private final Queue infixQueue;
    
    public EvaluatorEngineParenthesisMismatchTest(Queue infixQueue) {
        this.infixQueue = infixQueue;
    }
    
    @Test(expected=NonMatchingParenthesisException.class)
    public void testEvaluate() throws Exception {
        EvaluatorEngine evaluator = new EvaluatorEngine();
        evaluator.evaluate(infixQueue);
    }
    
}
