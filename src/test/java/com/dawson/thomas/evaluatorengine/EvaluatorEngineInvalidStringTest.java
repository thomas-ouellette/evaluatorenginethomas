package com.dawson.thomas.evaluatorengine;

import com.dawson.thomas.evaluatorengine.exceptions.InvalidStringException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 5 test cases for the InvalidStringException
 * 
 * @author Thomas
 */
@RunWith(Parameterized.class)
public class EvaluatorEngineInvalidStringTest {
    
    private static final Logger LOG = LoggerFactory.getLogger(EvaluatorEngineInvalidStringTest.class);
    
    @Parameterized.Parameters(name = "{index} expression[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("(", "0", "+", "0", ")", "/", ""))},
            {new ArrayDeque<>(Arrays.asList("2", "-", "3", "/", "     "))},
            {new ArrayDeque<>(Arrays.asList("5", "^", "(", "0", ")"))},
            {new ArrayDeque<>(Arrays.asList("5", "^", "((", "0", "))"))},
            {new ArrayDeque<>(Arrays.asList("5/", "/", "(", "5", ")"))}
        });
    }
    
    private final Queue infixQueue;
    
    public EvaluatorEngineInvalidStringTest(Queue infixQueue) {
        this.infixQueue = infixQueue;
    }
    
    @Test(expected=InvalidStringException.class)
    public void testEvaluate() throws Exception {
        EvaluatorEngine evaluator = new EvaluatorEngine();
        evaluator.evaluate(infixQueue);
    }
    
}
