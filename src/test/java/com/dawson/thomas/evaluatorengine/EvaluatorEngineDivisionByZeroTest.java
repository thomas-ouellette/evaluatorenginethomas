package com.dawson.thomas.evaluatorengine;

import com.dawson.thomas.evaluatorengine.exceptions.DivisionByZeroException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 3 test cases for the DivisionByZeroException
 * 
 * @author Thomas
 */
@RunWith(Parameterized.class)
public class EvaluatorEngineDivisionByZeroTest {
    
    private static final Logger LOG = LoggerFactory.getLogger(EvaluatorEngineDivisionByZeroTest.class);
    
    @Parameterized.Parameters(name = "{index} expression[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            
            {new ArrayDeque<>(Arrays.asList("(", "0", "+", "0", ")", "/", "0"))},
            {new ArrayDeque<>(Arrays.asList("2", "-", "3", "/", "0"))},
            {new ArrayDeque<>(Arrays.asList("5", "/", "(", "0", ")"))}
        });
    }
    
    private final Queue infixQueue;
    
    public EvaluatorEngineDivisionByZeroTest(Queue infixQueue) {
        this.infixQueue = infixQueue;
    }
    
    @Test(expected=DivisionByZeroException.class)
    public void testEvaluate() throws Exception {
        EvaluatorEngine evaluator = new EvaluatorEngine();
        evaluator.evaluate(infixQueue);
    }
    
}
