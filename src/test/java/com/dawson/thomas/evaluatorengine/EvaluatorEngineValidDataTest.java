package com.dawson.thomas.evaluatorengine;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *  Parameterized unit test class with 25 valid infix queues.
 * 
 * @author Thomas
 */
@RunWith(Parameterized.class)
public class EvaluatorEngineValidDataTest {
    
    private static final Logger LOG = LoggerFactory.getLogger(EvaluatorEngineValidDataTest.class);
    
    @Parameterized.Parameters(name = "{index} expression[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            
            {new ArrayDeque<>(Arrays.asList("2", "+", "3", "*", "4")), 14},
            {new ArrayDeque<>(Arrays.asList("2", "(", "3", "+", "2", ")")), 10},
            {new ArrayDeque<>(Arrays.asList("(", "5", "*", "25", "+", "(", "-5", ")", ")", "-", "20", "/", "2")), 110},
            {new ArrayDeque<>(Arrays.asList("(", "8", "-", "1", "+", "3", ")", "*", "6", "-", "(", "(", "3", "+", "7", ")", "*", "2", ")")), 40},
            {new ArrayDeque<>(Arrays.asList("(", "6", ")", "(", "9", ")")), 54},
            {new ArrayDeque<>(Arrays.asList("(", "(", "56", "/", "25", ")", "*", "33", ")", "/", "2")), 36.96},
            {new ArrayDeque<>(Arrays.asList("9", "(", "5", ")", "+", "60", "/", "20", "-", "8")), 40},
            {new ArrayDeque<>(Arrays.asList("4", "/", "5", "*", "8", "+", "7", "-", "5", "*", "5", "*", "8", "+", "55")), -131.6},
            {new ArrayDeque<>(Arrays.asList("45454", "/", "45494", "+", "54645", "-", "6457", "/", "(", "646464", "+", "464", ")")), 54645.98914},
            {new ArrayDeque<>(Arrays.asList("(", "5", ")", "/", "(", "5", ")", "/", "(", "5", ")")), 0.2},
            {new ArrayDeque<>(Arrays.asList("5", "(", "5", "(", "(", "(", "5", ")", ")", ")", ")")), 125},
            {new ArrayDeque<>(Arrays.asList("99", "+", "9", "+", "99", "*", "9", "+", "1")), 1000},
            {new ArrayDeque<>(Arrays.asList("66", "(", "(", "99", ")", ")", "-", "3", "/", "3")), 6533},
            {new ArrayDeque<>(Arrays.asList("(", "(", "33", ")", ")", "99", "(", "(", "66", ")", ")")), 215622},
            {new ArrayDeque<>(Arrays.asList("(", "(", "33", ")", ")", "99", "(", "(", "66", ")", ")", "/", "3", "/", "5")), 14374.8},
            {new ArrayDeque<>(Arrays.asList("94", "/", "26", "/", "72", "*", "491", "+", "76", "(", "975", ")")), 74124.65491},
            {new ArrayDeque<>(Arrays.asList("5", "-", "2", "+", "4", "*", "(", "8", "-", "(", "5", "+", "1", ")", ")", "+", "9")), 20},
            {new ArrayDeque<>(Arrays.asList("5.0E4", "/", "5")), 10000},
            {new ArrayDeque<>(Arrays.asList("4", "+", "5", "/", "5", "/", "2", "-", "1", "-", "4", "*", "42", "/", "48", "+", "416", "/", "46", "+", "48")), 57.04347826},
            {new ArrayDeque<>(Arrays.asList("10", "/", "3")), 3.33333},
            {new ArrayDeque<>(Arrays.asList("(", "(", "(", "(", "(", "5", "/", "6", ")", "-", "20", "+", "3", ")", "+", "60", ")", "-", "3", "+", "3", ")", "/", "66", ")", "-", "2")), -1.335858586},
            {new ArrayDeque<>(Arrays.asList("9", "/", "3", "+", "3", "+", "3", "+", "3", "(", "5", ")")), 24},
            {new ArrayDeque<>(Arrays.asList("175", "/", "4497", "-", "4691", "-", "544", "(", "9464", "/", "264", ")")), -24192.53684},
            {new ArrayDeque<>(Arrays.asList("454", "+", "4644", "/", "649", "-", "4797", "+", "92", "/", "6")), -4320.511043},
            {new ArrayDeque<>(Arrays.asList("2", "/", "4", "*", "8", "-", "1", "(", "45", "/", "15", "-", "45", ")", "-", "45", "/", "45", "*", "464")), -418}
        });
    }
    
    private final Queue infixQueue;
    private final double expectedResult;
    
    public EvaluatorEngineValidDataTest(Queue infixQueue, double expectedResult) {
        this.infixQueue = infixQueue;
        this.expectedResult = expectedResult;
    }
    
    @Test
    public void testEvaluate() throws Exception {
        EvaluatorEngine evaluator = new EvaluatorEngine();
        double result = Double.parseDouble(evaluator.evaluate(infixQueue));
        assertEquals(result, expectedResult, 0.0001);
    }
    
}
