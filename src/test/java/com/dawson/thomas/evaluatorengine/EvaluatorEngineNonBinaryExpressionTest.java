/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dawson.thomas.evaluatorengine;

import com.dawson.thomas.evaluatorengine.exceptions.NonBinaryExpressionException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 5 test cases for the NonBinaryExpressionException
 * 
 * @author Thomas
 */
@RunWith(Parameterized.class)
public class EvaluatorEngineNonBinaryExpressionTest {
    
    private static final Logger LOG = LoggerFactory.getLogger(EvaluatorEngineNonBinaryExpressionTest.class);
    
    @Parameterized.Parameters(name = "{index} expression[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            
            {new ArrayDeque<>(Arrays.asList("2", "+", "4", "*"))},
            {new ArrayDeque<>(Arrays.asList("+", "5", "*", "20", "+"))},
            {new ArrayDeque<>(Arrays.asList("5", "/", "/", "2"))},
            {new ArrayDeque<>(Arrays.asList("5", "+", "(", ")"))},
            {new ArrayDeque<>(Arrays.asList("5", "/", "(", "+", ")"))}
        });
    }
    
    private final Queue infixQueue;
    
    public EvaluatorEngineNonBinaryExpressionTest(Queue infixQueue) {
        this.infixQueue = infixQueue;
    }
    
    @Test(expected=NonBinaryExpressionException.class)
    public void testEvaluate() throws Exception {
        EvaluatorEngine evaluator = new EvaluatorEngine();
        evaluator.evaluate(infixQueue);
    }
    
}
